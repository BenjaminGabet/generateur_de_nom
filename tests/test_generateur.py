import unittest
from generateur import Verif_Donnees
from generateur import Traitement

class TestGenerateurFunctions(unittest.TestCase):

    def test_verif_donnees(self):
        self.assertTrue(Verif_Donnees("Gabet", "Benjamin", "StarWars"))
        self.assertFalse(Verif_Donnees("", "b", "StarWars"))
        self.assertFalse(Verif_Donnees("a", "d", "Dragon"))
    
    def test_traitement(self):
        self.assertEqual(Traitement("GABET", "BENJAMIN", "Dragon"), "Wicked Blaze")
        self.assertEqual(Traitement("GABET", "BENJAMIN", "StarWars"), "Padamé Sidious")
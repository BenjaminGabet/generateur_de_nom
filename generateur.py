from tkinter.messagebox import *
from tkinter import * 
from tkinter import tix

NBUNIVERS = 3

dict_correspondance = {
    "StarWars": {"nbFirstName" : 1, 
                "nbLastName" : 3,
                "A" : {1: "Darth", 2: "Bane"},
                "B" : {1: "Padamé", 2: "Sidious"},
                "C" : {1: "Jar Jar", 2: "Skywalker"},
                "D" : {1: "Obi-wan", 2: "Chewbacca"},
                "E" : {1: "Emperor", 2: "Fett"},
                "F" : {1: "Han", 2: "Trooper"},
                "G" : {1: "Greedo", 2: "PO"},
                "H" : {1: "Anakin", 2: "Speeder"},
                "I" : {1: "Count", 2: "Dooku"},
                "J" : {1: "Sarlacc", 2: "Pit"},
                "K" : {1: "Jabba", 2: "Vader"},
                "L" : {1: "Sergeant", 2: "D2"},
                "M" : {1: "Boba", 2: "Palpatine"},
                "N" : {1: "Witchet W.", 2: "Grievous"},
                "O" : {1: "Wampa", 2: "Binks"},
                "P" : {1: "Semator", 2: "Soldier"},
                "Q" : {1: "Queen", 2: "Kenobi"},
                "R" : {1: "Padawan", 2: "Solo"},
                "S" : {1: "Imperial", 2: "The Hutt"},
                "T" : {1: "General", 2: "Maul"},
                "U" : {1: "Storm", 2: "Windu"},
                "V" : {1: "R2", 2: "Queen"},
                "W" : {1: "Luke", 2: "Leia"},
                "X" : {1: "C3", 2: "Monster"},
                "Y" : {1: "Mace", 2: "Yoda"},
                "Z" : {1: "Princess", 2: "Warrick"},
                },
    "Games of Thrones": {"nbFirstName" : 1, 
                "nbLastName" : 1,
                "A" : {1: "Bloody", 2: "Heartstriker"},
                "B" : {1: "Vulgar", 2: "Nightfall"},
                "C" : {1: "Maddening", 2: "Vagabond"},
                "D" : {1: "Ragged", 2: "Oath"},
                "E" : {1: "Sordid", 2: "Judge"},
                "F" : {1: "Mortal", 2: "Breaker"},
                "G" : {1: "Barbarous", 2: "Mayhem"},
                "H" : {1: "Subborn", 2: "Spirit"},
                "I" : {1: "Cruel", 2: "Commander"},
                "J" : {1: "Bitter", 2: "Marvel"},
                "K" : {1: "Jagged", 2: "Fire"},
                "L" : {1: "Haunted", 2: "Destroyer"},
                "M" : {1: "Sovereign", 2: "Widowmaker"},
                "N" : {1: "Crimson", 2: "Vengance"},
                "O" : {1: "Mighty", 2: "Storm"},
                "P" : {1: "Ferral", 2: "Flame"},
                "Q" : {1: "Imperial", 2: "Blade"},
                "R" : {1: "Jagged", 2: "Chaos"},
                "S" : {1: "Lethal", 2: "Giant"},
                "T" : {1: "Ferocious", 2: "Steel"},
                "U" : {1: "Intrepid", 2: "Thunder"},
                "V" : {1: "Somber", 2: "Hellhound"},
                "W" : {1: "Maddening", 2: "Butcher"},
                "X" : {1: "Ruthless", 2: "Chaos"},
                "Y" : {1: "Sacred", 2: "Sorrow"},
                "Z" : {1: "Unforgiving", 2: "Jammer"},
                },
    "Dragon": {"nbFirstName" : 3, 
                "nbLastName" : 2,
                "A" : {1: "Regal", 2: "Blaze"},
                "B" : {1: "Viscious", 2: "Fire-eater"},
                "C" : {1: "Savage", 2: "Warrior"},
                "D" : {1: "Dark", 2: "Inferno"},
                "E" : {1: "Merciless", 2: "Shadow"},
                "F" : {1: "Legendary", 2: "Slayer"},
                "G" : {1: "Ancient", 2: "Storm"},
                "H" : {1: "Fearsome", 2: "Hunter"},
                "I" : {1: "Cruel", 2: "Illusion"},
                "J" : {1: "Magnificent", 2: "Siren"},
                "K" : {1: "Graceful", 2: "Serpent"},
                "L" : {1: "Sealed", 2: "Hell-raiser"},
                "M" : {1: "Noble", 2: "Barbarian"},
                "N" : {1: "Wicked", 2: "Rapture"},
                "O" : {1: "Majestic", 2: "Beast"},
                "P" : {1: "Scarred", 2: "Vengeance"},
                "Q" : {1: "Brutal", 2: "Rage"},
                "R" : {1: "Winged", 2: "Rogue"},
                "S" : {1: "Majestic", 2: "Venom"},
                "T" : {1: "Magical", 2: "Hellion"},
                "U" : {1: "Glorious", 2: "Spirit"},
                "V" : {1: "Hydra", 2: "Demon"},
                "W" : {1: "Valiant", 2: "Chaos"},
                "X" : {1: "Ferocious", 2: "Fire"},
                "Y" : {1: "Deadly", 2: "Legend"},
                "Z" : {1: "Cunning", 2: "Creature"},
                }
}

#Evenement au clic de la zone de texte de saisie qui efface le contenu
def onClick(event):
    zone_Nom_Saisie.delete(0, len(zone_Nom_Saisie.get()))

#Evenement au clic de la zone de texte de saisie qui efface le contenu
def onClick2(event):
    zone_Prenom_Saisie.delete(0, len(zone_Prenom_Saisie.get()))

'''
Verif_Donnees : Compare la taille du nom et du prenom pour savoir s'il y a suffisament de lettre pour pouvoir traiter le generateur
avec le bon univers.
IN : Le nom, le prenom et le choix de l'univers saisis par l'utilisateur
OUT : Booléen pour être sûr que les valeurs sont bonnes
'''
def Verif_Donnees(nom, prenom, choix_univers):
    if(len(nom) < dict_correspondance[choix_univers]["nbLastName"]):
        return False
    if(len(prenom) < dict_correspondance[choix_univers]["nbFirstName"]):
        return False
    return True

'''
Traitement : Traite les données saisies avec les dictionnaires pour créer la nouvelle identité
IN : Le nom, le prenom et le choix de l'univers saisis par l'utilisateur
OUT : La nouvelle identité de l'utilisateur
'''
def Traitement(nom, prenom, choix_univers):
    identite = ""
    identite = identite + dict_correspondance[choix_univers][prenom[dict_correspondance[choix_univers]["nbFirstName"]-1]][1] + " "
    identite = identite + dict_correspondance[choix_univers][nom[dict_correspondance[choix_univers]["nbLastName"]-1]][2]
    return identite

'''
Affichage : Change la question et affiche la réponse que le programme doit faire
IN : L'identité et le choix de l'univers saisis par l'utilisateur
OUT : L'affichage de la bonne phrase et du nouveau nom
'''
def Affichage(identite, choix_univers):
    zone_Texte_Question.config(text="What is your {} name ?".format(choix_univers))
    zone_Texte_Reponse.config(text= identite)

'''
Principal : Permet d'afficher une erreur en cas de problème avec l'univers choisi
'''
def Principal():
    if(Verif_Donnees(nom_Saisie.get(), prenom_Saisie.get(), valeur_Choix_Univers.get())):
        identite = Traitement(nom_Saisie.get().upper(), prenom_Saisie.get().upper(), valeur_Choix_Univers.get())
        Affichage(identite, valeur_Choix_Univers.get())
    else:
        showwarning('Saisie', 'Veuillez rentrer un prénom et un nom de longueur suffisante')

if __name__=="__main__":
    root = tix.Tk()
    root.title("Generateur de Nom")
    root.rowconfigure(2, weight=1)
    root.columnconfigure(1, weight=1)

    nom_Saisie = StringVar()
    nom_Saisie.set("Entrez votre nom de famille")
    prenom_Saisie = StringVar()
    prenom_Saisie.set("Entrez votre prénom")
    valeur_Choix_Univers = StringVar()
    valeur_Choix_Univers.set("StarWars")
    liste_Cle_Dict = list(dict_correspondance.keys())

    #Zone de texte pour saisir le nom
    zone_Nom_Saisie = Entry(root, textvariable = nom_Saisie)
    zone_Nom_Saisie.grid(column=0, row=0)
    zone_Nom_Saisie.bind('<Button-1>', onClick)

    #Zone de texte pour saisir le prenom
    zone_Prenom_Saisie = Entry(root, textvariable = prenom_Saisie)
    zone_Prenom_Saisie.grid(column=1, row=0)
    zone_Prenom_Saisie.bind('<Button-1>', onClick2)

    #Combobox pour le choix de l'univers.
    combo = tix.ComboBox(root, editable=0, dropdown=1, variable=valeur_Choix_Univers)
    for indice in range(NBUNIVERS):
        combo.insert(indice, liste_Cle_Dict[indice]) 
    combo.grid(column=0, row=1)  

    #Bouton validation pour lancer le traitement et la vérification
    boutton_validation = Button(root, text="Valider", command = Principal)
    boutton_validation.grid(column=1, row=1)

    #Zone d'affichage de la question
    zone_Texte_Question = Label(root, text= " ")
    zone_Texte_Question.grid(column=0 , row=2)

    #Zone d'affichage pour la réponse
    zone_Texte_Reponse = Label(root, text= " ")
    zone_Texte_Reponse.grid(column=2 , row=2)

    root.mainloop()